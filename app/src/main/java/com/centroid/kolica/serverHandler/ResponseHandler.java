package com.centroid.kolica.serverHandler;

public interface ResponseHandler {


    public void onSuccess(String data);

    public void onFailure(String err);


}
