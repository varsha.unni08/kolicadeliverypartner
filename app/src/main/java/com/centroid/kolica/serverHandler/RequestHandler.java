package com.centroid.kolica.serverHandler;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.NoCache;
import com.android.volley.toolbox.StringRequest;

import com.centroid.kolica.UserAlerts;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.preferencehelper.PreferenceHelper;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class RequestHandler {

    Context context;
    Map<String,String>params;
    ResponseHandler responseHandler;
    String methodename;
    int methodetype;
    String url ="";
    public RequestHandler(Context context, Map<String, String> params, ResponseHandler responseHandler, String methodename, int methodetype) {
        this.context = context;
        this.params = params;
        this.responseHandler = responseHandler;
        this.methodename = methodename;
        this.methodetype = methodetype;
    }

    public void submitRequest(){

        if(!UserAlerts.isConnectionAvailable(context))
        {

            responseHandler.onFailure("Check your internet connection......");

        }







        RequestQueue requestQueue;

// Instantiate the cache


// Instantiate the RequestQueue with the cache and network.
      //  requestQueue = Volley.newRequestQueue(context);
        requestQueue = new RequestQueue(new NoCache(), new BasicNetwork(new HurlStack()));
        requestQueue.getCache().clear();

// Start the queue
        requestQueue.start();


       // if(!methodename.startsWith(Utils.WebServiceMethodes.smsMethode)) {


             url = ServerData.baseurl + methodename;









// Formulate the request and handle the response.
        StringRequest stringRequest = new StringRequest(methodetype, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        if(responseHandler!=null)
                        {

                            if(response!=null) {

                                responseHandler.onSuccess(response);
                            }
                        }



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Handle error

                        Log.e("Errormethode",url);



                        if(responseHandler!=null)
                        {



                            if(!UserAlerts.isConnectionAvailable(context))
                            {


                                Snackbar snackbar = Snackbar.make(((AppCompatActivity)context).findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG);


                                snackbar.show();

                               // responseHandler.onFailure("Check your internet connection");


                            }
                            else {


                                if (error != null) {

                                    try {

                                        if (error.networkResponse != null) {
                                            if (error.networkResponse.data != null) {
                                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                                JSONObject data = new JSONObject(responseBody);
                                                JSONArray errors = data.getJSONArray("errors");
                                                JSONObject jsonMessage = errors.getJSONObject(0);
                                                String message = jsonMessage.getString("message");

                                                responseHandler.onFailure("Network Error");
                                            }
                                        }
                                        // Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                    } catch (JSONException e) {

                                        responseHandler.onFailure(error.toString());

                                    } catch (UnsupportedEncodingException err) {

                                        responseHandler.onFailure(error.toString());


                                    }

                                }
                            }
                        }

                    }
                })
        {

            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String,String> params = new HashMap<>();

                params.put("Content-Type","application/x-www-form-urlencoded");
                params.put("Authorization",new PreferenceHelper(context).getData(ServerData.userkey));


                return params;
            }



        }







                ;

// Add the request to the RequestQueue.
        stringRequest.setShouldCache(false);
        requestQueue.getCache().clear();




        RetryPolicy mRetryPolicy = new DefaultRetryPolicy(
                3000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(mRetryPolicy);



        requestQueue.add(stringRequest);




    }
}
