package com.centroid.kolica.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("addressid")
    @Expose
    private String addressid;
    @SerializedName("order_type")
    @Expose
    private String orderType;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("item_count")
    @Expose
    private String itemCount;
    @SerializedName("expected_deli_date")
    @Expose
    private String expectedDeliDate;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("delevery_charge")
    @Expose
    private String deleveryCharge;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discounted_total_price")
    @Expose
    private String discountedTotalPrice;
    @SerializedName("cancel_status")
    @Expose
    private String cancelStatus;
    @SerializedName("delivery_completion_status")
    @Expose
    private String deliveryCompletionStatus;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("sale_status")
    @Expose
    private String saleStatus;
    @SerializedName("payment_methode")
    @Expose
    private String paymentMethode;
    @SerializedName("transactionid")
    @Expose
    private Object transactionid;

    public OrderData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAddressid() {
        return addressid;
    }

    public void setAddressid(String addressid) {
        this.addressid = addressid;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public String getExpectedDeliDate() {
        return expectedDeliDate;
    }

    public void setExpectedDeliDate(String expectedDeliDate) {
        this.expectedDeliDate = expectedDeliDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDeleveryCharge() {
        return deleveryCharge;
    }

    public void setDeleveryCharge(String deleveryCharge) {
        this.deleveryCharge = deleveryCharge;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountedTotalPrice() {
        return discountedTotalPrice;
    }

    public void setDiscountedTotalPrice(String discountedTotalPrice) {
        this.discountedTotalPrice = discountedTotalPrice;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getDeliveryCompletionStatus() {
        return deliveryCompletionStatus;
    }

    public void setDeliveryCompletionStatus(String deliveryCompletionStatus) {
        this.deliveryCompletionStatus = deliveryCompletionStatus;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(String saleStatus) {
        this.saleStatus = saleStatus;
    }

    public String getPaymentMethode() {
        return paymentMethode;
    }

    public void setPaymentMethode(String paymentMethode) {
        this.paymentMethode = paymentMethode;
    }

    public Object getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(Object transactionid) {
        this.transactionid = transactionid;
    }
}
