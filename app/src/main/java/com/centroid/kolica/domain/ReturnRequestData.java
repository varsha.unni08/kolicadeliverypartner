package com.centroid.kolica.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReturnRequestData {

    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("addressid")
    @Expose
    private String addressid;
    @SerializedName("order_type")
    @Expose
    private String orderType;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("item_count")
    @Expose
    private String itemCount;
    @SerializedName("expected_deli_date")
    @Expose
    private String expectedDeliDate;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("delevery_charge")
    @Expose
    private String deleveryCharge;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("discounted_total_price")
    @Expose
    private String discountedTotalPrice;
    @SerializedName("cancel_status")
    @Expose
    private String cancelStatus;
    @SerializedName("delivery_completion_status")
    @Expose
    private String deliveryCompletionStatus;
    @SerializedName("comments")
    @Expose
    private String comments;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("sale_status")
    @Expose
    private String saleStatus;
    @SerializedName("deliveryboy_id")
    @Expose
    private String deliveryboyId;
    @SerializedName("payment_methode")
    @Expose
    private String paymentMethode;
    @SerializedName("isPaymentUPi")
    @Expose
    private String isPaymentUPi;
    @SerializedName("transactionid")
    @Expose
    private Object transactionid;
    @SerializedName("OrderDetails_ID")
    @Expose
    private String orderDetailsID;
    @SerializedName("shopid")
    @Expose
    private String shopid;
    @SerializedName("stockid")
    @Expose
    private String stockid;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("qty_to_deliver")
    @Expose
    private String qtyToDeliver;
    @SerializedName("deliverydate")
    @Expose
    private String deliverydate;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("return_date")
    @Expose
    private Object returnDate;
    @SerializedName("return_qty")
    @Expose
    private String returnQty;
    @SerializedName("return_status")
    @Expose
    private String returnStatus;
    @SerializedName("return_request_status")
    @Expose
    private String returnRequestStatus;
    @SerializedName("return_by_deliveryboy")
    @Expose
    private String returnByDeliveryboy;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("cash_back")
    @Expose
    private String cashBack;
    @SerializedName("feedback")
    @Expose
    private Object feedback;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("invoice_status")
    @Expose
    private String invoiceStatus;
    @SerializedName("cartid")
    @Expose
    private String cartid;
    @SerializedName("productid")
    @Expose
    private String productid;
    @SerializedName("sub_catid")
    @Expose
    private String subCatid;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isreturnable")
    @Expose
    private String isreturnable;
    @SerializedName("make")
    @Expose
    private Object make;
    @SerializedName("height")
    @Expose
    private Object height;
    @SerializedName("width")
    @Expose
    private Object width;
    @SerializedName("weight")
    @Expose
    private Object weight;
    @SerializedName("size")
    @Expose
    private Object size;
    @SerializedName("thickness")
    @Expose
    private Object thickness;
    @SerializedName("color")
    @Expose
    private Object color;
    @SerializedName("sizedifference")
    @Expose
    private Object sizedifference;
    @SerializedName("image1")
    @Expose
    private String image1;
    @SerializedName("image2")
    @Expose
    private String image2;
    @SerializedName("image3")
    @Expose
    private String image3;
    @SerializedName("image4")
    @Expose
    private String image4;
    @SerializedName("image5")
    @Expose
    private String image5;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("batchno")
    @Expose
    private String batchno;
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("warehouse_id")
    @Expose
    private String warehouseId;
    @SerializedName("distributor_id")
    @Expose
    private String distributorId;
    @SerializedName("make_date")
    @Expose
    private String makeDate;
    @SerializedName("expe_date")
    @Expose
    private String expeDate;
    @SerializedName("qty_stock")
    @Expose
    private String qtyStock;
    @SerializedName("qty_damage")
    @Expose
    private String qtyDamage;
    @SerializedName("qty_rol")
    @Expose
    private String qtyRol;
    @SerializedName("discounted_price")
    @Expose
    private String discountedPrice;
    @SerializedName("isOffer")
    @Expose
    private String isOffer;
    @SerializedName("offer_percent")
    @Expose
    private String offerPercent;
    @SerializedName("isCashBack")
    @Expose
    private String isCashBack;
    @SerializedName("cashback_amount")
    @Expose
    private String cashbackAmount;
    @SerializedName("total_with_gst")
    @Expose
    private String totalWithGst;
    @SerializedName("stock_status")
    @Expose
    private String stockStatus;
    @SerializedName("iswide")
    @Expose
    private String iswide;
    @SerializedName("stock_update_date")
    @Expose
    private String stockUpdateDate;
    @SerializedName("commition_agent_id")
    @Expose
    private Object commitionAgentId;
    @SerializedName("added_user_id")
    @Expose
    private String addedUserId;
    @SerializedName("barcode_no")
    @Expose
    private String barcodeNo;
    @SerializedName("purchase_without_gst")
    @Expose
    private String purchaseWithoutGst;
    @SerializedName("purchase_with_tax")
    @Expose
    private String purchaseWithTax;
    @SerializedName("MRP")
    @Expose
    private String mrp;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("cashback_percent")
    @Expose
    private String cashbackPercent;
    @SerializedName("salespri_without_gst")
    @Expose
    private String salespriWithoutGst;
    @SerializedName("salespri_with_gst")
    @Expose
    private String salespriWithGst;
    @SerializedName("reff_income_percent")
    @Expose
    private String reffIncomePercent;
    @SerializedName("reff_gen_status")
    @Expose
    private String reffGenStatus;
    @SerializedName("cashback_gen_status")
    @Expose
    private String cashbackGenStatus;

    public ReturnRequestData() {
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getAddressid() {
        return addressid;
    }

    public void setAddressid(String addressid) {
        this.addressid = addressid;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public String getExpectedDeliDate() {
        return expectedDeliDate;
    }

    public void setExpectedDeliDate(String expectedDeliDate) {
        this.expectedDeliDate = expectedDeliDate;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDeleveryCharge() {
        return deleveryCharge;
    }

    public void setDeleveryCharge(String deleveryCharge) {
        this.deleveryCharge = deleveryCharge;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscountedTotalPrice() {
        return discountedTotalPrice;
    }

    public void setDiscountedTotalPrice(String discountedTotalPrice) {
        this.discountedTotalPrice = discountedTotalPrice;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getDeliveryCompletionStatus() {
        return deliveryCompletionStatus;
    }

    public void setDeliveryCompletionStatus(String deliveryCompletionStatus) {
        this.deliveryCompletionStatus = deliveryCompletionStatus;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(String saleStatus) {
        this.saleStatus = saleStatus;
    }

    public String getDeliveryboyId() {
        return deliveryboyId;
    }

    public void setDeliveryboyId(String deliveryboyId) {
        this.deliveryboyId = deliveryboyId;
    }

    public String getPaymentMethode() {
        return paymentMethode;
    }

    public void setPaymentMethode(String paymentMethode) {
        this.paymentMethode = paymentMethode;
    }

    public String getIsPaymentUPi() {
        return isPaymentUPi;
    }

    public void setIsPaymentUPi(String isPaymentUPi) {
        this.isPaymentUPi = isPaymentUPi;
    }

    public Object getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(Object transactionid) {
        this.transactionid = transactionid;
    }

    public String getOrderDetailsID() {
        return orderDetailsID;
    }

    public void setOrderDetailsID(String orderDetailsID) {
        this.orderDetailsID = orderDetailsID;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    public String getStockid() {
        return stockid;
    }

    public void setStockid(String stockid) {
        this.stockid = stockid;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQtyToDeliver() {
        return qtyToDeliver;
    }

    public void setQtyToDeliver(String qtyToDeliver) {
        this.qtyToDeliver = qtyToDeliver;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(String deliverydate) {
        this.deliverydate = deliverydate;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public Object getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Object returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnQty() {
        return returnQty;
    }

    public void setReturnQty(String returnQty) {
        this.returnQty = returnQty;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getReturnRequestStatus() {
        return returnRequestStatus;
    }

    public void setReturnRequestStatus(String returnRequestStatus) {
        this.returnRequestStatus = returnRequestStatus;
    }

    public String getReturnByDeliveryboy() {
        return returnByDeliveryboy;
    }

    public void setReturnByDeliveryboy(String returnByDeliveryboy) {
        this.returnByDeliveryboy = returnByDeliveryboy;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getCashBack() {
        return cashBack;
    }

    public void setCashBack(String cashBack) {
        this.cashBack = cashBack;
    }

    public Object getFeedback() {
        return feedback;
    }

    public void setFeedback(Object feedback) {
        this.feedback = feedback;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getCartid() {
        return cartid;
    }

    public void setCartid(String cartid) {
        this.cartid = cartid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getSubCatid() {
        return subCatid;
    }

    public void setSubCatid(String subCatid) {
        this.subCatid = subCatid;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsreturnable() {
        return isreturnable;
    }

    public void setIsreturnable(String isreturnable) {
        this.isreturnable = isreturnable;
    }

    public Object getMake() {
        return make;
    }

    public void setMake(Object make) {
        this.make = make;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(Object weight) {
        this.weight = weight;
    }

    public Object getSize() {
        return size;
    }

    public void setSize(Object size) {
        this.size = size;
    }

    public Object getThickness() {
        return thickness;
    }

    public void setThickness(Object thickness) {
        this.thickness = thickness;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Object getSizedifference() {
        return sizedifference;
    }

    public void setSizedifference(Object sizedifference) {
        this.sizedifference = sizedifference;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getExpeDate() {
        return expeDate;
    }

    public void setExpeDate(String expeDate) {
        this.expeDate = expeDate;
    }

    public String getQtyStock() {
        return qtyStock;
    }

    public void setQtyStock(String qtyStock) {
        this.qtyStock = qtyStock;
    }

    public String getQtyDamage() {
        return qtyDamage;
    }

    public void setQtyDamage(String qtyDamage) {
        this.qtyDamage = qtyDamage;
    }

    public String getQtyRol() {
        return qtyRol;
    }

    public void setQtyRol(String qtyRol) {
        this.qtyRol = qtyRol;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getIsOffer() {
        return isOffer;
    }

    public void setIsOffer(String isOffer) {
        this.isOffer = isOffer;
    }

    public String getOfferPercent() {
        return offerPercent;
    }

    public void setOfferPercent(String offerPercent) {
        this.offerPercent = offerPercent;
    }

    public String getIsCashBack() {
        return isCashBack;
    }

    public void setIsCashBack(String isCashBack) {
        this.isCashBack = isCashBack;
    }

    public String getCashbackAmount() {
        return cashbackAmount;
    }

    public void setCashbackAmount(String cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }

    public String getTotalWithGst() {
        return totalWithGst;
    }

    public void setTotalWithGst(String totalWithGst) {
        this.totalWithGst = totalWithGst;
    }

    public String getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getIswide() {
        return iswide;
    }

    public void setIswide(String iswide) {
        this.iswide = iswide;
    }

    public String getStockUpdateDate() {
        return stockUpdateDate;
    }

    public void setStockUpdateDate(String stockUpdateDate) {
        this.stockUpdateDate = stockUpdateDate;
    }

    public Object getCommitionAgentId() {
        return commitionAgentId;
    }

    public void setCommitionAgentId(Object commitionAgentId) {
        this.commitionAgentId = commitionAgentId;
    }

    public String getAddedUserId() {
        return addedUserId;
    }

    public void setAddedUserId(String addedUserId) {
        this.addedUserId = addedUserId;
    }

    public String getBarcodeNo() {
        return barcodeNo;
    }

    public void setBarcodeNo(String barcodeNo) {
        this.barcodeNo = barcodeNo;
    }

    public String getPurchaseWithoutGst() {
        return purchaseWithoutGst;
    }

    public void setPurchaseWithoutGst(String purchaseWithoutGst) {
        this.purchaseWithoutGst = purchaseWithoutGst;
    }

    public String getPurchaseWithTax() {
        return purchaseWithTax;
    }

    public void setPurchaseWithTax(String purchaseWithTax) {
        this.purchaseWithTax = purchaseWithTax;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getCashbackPercent() {
        return cashbackPercent;
    }

    public void setCashbackPercent(String cashbackPercent) {
        this.cashbackPercent = cashbackPercent;
    }

    public String getSalespriWithoutGst() {
        return salespriWithoutGst;
    }

    public void setSalespriWithoutGst(String salespriWithoutGst) {
        this.salespriWithoutGst = salespriWithoutGst;
    }

    public String getSalespriWithGst() {
        return salespriWithGst;
    }

    public void setSalespriWithGst(String salespriWithGst) {
        this.salespriWithGst = salespriWithGst;
    }

    public String getReffIncomePercent() {
        return reffIncomePercent;
    }

    public void setReffIncomePercent(String reffIncomePercent) {
        this.reffIncomePercent = reffIncomePercent;
    }

    public String getReffGenStatus() {
        return reffGenStatus;
    }

    public void setReffGenStatus(String reffGenStatus) {
        this.reffGenStatus = reffGenStatus;
    }

    public String getCashbackGenStatus() {
        return cashbackGenStatus;
    }

    public void setCashbackGenStatus(String cashbackGenStatus) {
        this.cashbackGenStatus = cashbackGenStatus;
    }
}
