package com.centroid.kolica.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingsData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("deliverycharge")
    @Expose
    private String deliverycharge;
    @SerializedName("referal_percentage")
    @Expose
    private String referalPercentage;
    @SerializedName("app_version")
    @Expose
    private String appVersion;
    @SerializedName("offerpercentage")
    @Expose
    private String offerpercentage;
    @SerializedName("free_deliveryamount")
    @Expose
    private String freeDeliveryamount;
    @SerializedName("deliverykm")
    @Expose
    private String deliverykm;
    @SerializedName("amount_credit_period")
    @Expose
    private String amountCreditPeriod;
    @SerializedName("invoice_prefix")
    @Expose
    private String invoicePrefix;
    @SerializedName("customerCar_phone")
    @Expose
    private String customerCarPhone;

    public SettingsData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDeliverycharge() {
        return deliverycharge;
    }

    public void setDeliverycharge(String deliverycharge) {
        this.deliverycharge = deliverycharge;
    }

    public String getReferalPercentage() {
        return referalPercentage;
    }

    public void setReferalPercentage(String referalPercentage) {
        this.referalPercentage = referalPercentage;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getOfferpercentage() {
        return offerpercentage;
    }

    public void setOfferpercentage(String offerpercentage) {
        this.offerpercentage = offerpercentage;
    }

    public String getFreeDeliveryamount() {
        return freeDeliveryamount;
    }

    public void setFreeDeliveryamount(String freeDeliveryamount) {
        this.freeDeliveryamount = freeDeliveryamount;
    }

    public String getDeliverykm() {
        return deliverykm;
    }

    public void setDeliverykm(String deliverykm) {
        this.deliverykm = deliverykm;
    }

    public String getAmountCreditPeriod() {
        return amountCreditPeriod;
    }

    public void setAmountCreditPeriod(String amountCreditPeriod) {
        this.amountCreditPeriod = amountCreditPeriod;
    }

    public String getInvoicePrefix() {
        return invoicePrefix;
    }

    public void setInvoicePrefix(String invoicePrefix) {
        this.invoicePrefix = invoicePrefix;
    }

    public String getCustomerCarPhone() {
        return customerCarPhone;
    }

    public void setCustomerCarPhone(String customerCarPhone) {
        this.customerCarPhone = customerCarPhone;
    }
}
