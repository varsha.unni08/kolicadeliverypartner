package com.centroid.kolica.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductOrderData {


    @SerializedName("sales_details_id")
    @Expose
    private String salesDetailsId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("shopid")
    @Expose
    private String shopid;
    @SerializedName("stockid")
    @Expose
    private String stockid;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("qty_to_deliver")
    @Expose
    private String qtyToDeliver;
    @SerializedName("deliverydate")
    @Expose
    private String deliverydate;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("cancel_status")
    @Expose
    private String cancelStatus;
    @SerializedName("return_date")
    @Expose
    private Object returnDate;
    @SerializedName("return_status")
    @Expose
    private String returnStatus;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("cash_back")
    @Expose
    private String cashBack;
    @SerializedName("feedback")
    @Expose
    private Object feedback;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("invoice_status")
    @Expose
    private String invoiceStatus;
    @SerializedName("cartid")
    @Expose
    private String cartid;
    @SerializedName("productid")
    @Expose
    private String productid;
    @SerializedName("sub_catid")
    @Expose
    private String subCatid;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("isreturnable")
    @Expose
    private String isreturnable;
    @SerializedName("make")
    @Expose
    private Object make;
    @SerializedName("height")
    @Expose
    private Object height;
    @SerializedName("width")
    @Expose
    private Object width;
    @SerializedName("weight")
    @Expose
    private Object weight;
    @SerializedName("size")
    @Expose
    private Object size;
    @SerializedName("thickness")
    @Expose
    private Object thickness;
    @SerializedName("color")
    @Expose
    private Object color;
    @SerializedName("sizedifference")
    @Expose
    private Object sizedifference;
    @SerializedName("image1")
    @Expose
    private Object image1;
    @SerializedName("image2")
    @Expose
    private Object image2;
    @SerializedName("image3")
    @Expose
    private Object image3;
    @SerializedName("image4")
    @Expose
    private Object image4;
    @SerializedName("image5")
    @Expose
    private Object image5;
    @SerializedName("batchno")
    @Expose
    private String batchno;
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("warehouse_id")
    @Expose
    private String warehouseId;
    @SerializedName("distributor_id")
    @Expose
    private String distributorId;
    @SerializedName("make_date")
    @Expose
    private String makeDate;
    @SerializedName("expe_date")
    @Expose
    private String expeDate;
    @SerializedName("qty_stock")
    @Expose
    private String qtyStock;
    @SerializedName("qty_damage")
    @Expose
    private String qtyDamage;
    @SerializedName("qty_rol")
    @Expose
    private String qtyRol;
    @SerializedName("discounted_price")
    @Expose
    private String discountedPrice;
    @SerializedName("isOffer")
    @Expose
    private String isOffer;
    @SerializedName("offer_percent")
    @Expose
    private String offerPercent;
    @SerializedName("isCashBack")
    @Expose
    private String isCashBack;
    @SerializedName("cashback_amount")
    @Expose
    private String cashbackAmount;
    @SerializedName("total_with_gst")
    @Expose
    private String totalWithGst;
    @SerializedName("stock_status")
    @Expose
    private String stockStatus;
    @SerializedName("iswide")
    @Expose
    private String iswide;
    @SerializedName("stock_update_date")
    @Expose
    private String stockUpdateDate;
    @SerializedName("commition_agent_id")
    @Expose
    private Object commitionAgentId;
    @SerializedName("added_user_id")
    @Expose
    private String addedUserId;
    @SerializedName("barcode_no")
    @Expose
    private String barcodeNo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("purchase_without_gst")
    @Expose
    private String purchaseWithoutGst;
    @SerializedName("purchase_with_tax")
    @Expose
    private String purchaseWithTax;
    @SerializedName("MRP")
    @Expose
    private String mrp;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("cashback_percent")
    @Expose
    private String cashbackPercent;
    @SerializedName("salespri_without_gst")
    @Expose
    private String salespriWithoutGst;
    @SerializedName("salespri_with_gst")
    @Expose
    private String salespriWithGst;
    @SerializedName("reff_income_percent")
    @Expose
    private String reffIncomePercent;
    @SerializedName("reff_gen_status")
    @Expose
    private String reffGenStatus;
    @SerializedName("cashback_gen_status")
    @Expose
    private String cashbackGenStatus;
    @SerializedName("customerid")
    @Expose
    private String customerid;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("phone1")
    @Expose
    private String phone1;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("refered_userid")
    @Expose
    private String referedUserid;
    @SerializedName("district")
    @Expose
    private String district;

    int selected=0;

    int quantity_updated=0;

    int isdamaged=0;

    int qtytobill=0;

    public ProductOrderData() {
    }

    public int getQtytobill() {
        return qtytobill;
    }

    public void setQtytobill(int qtytobill) {
        this.qtytobill = qtytobill;
    }

    public int getIsdamaged() {
        return isdamaged;
    }

    public void setIsdamaged(int isdamaged) {
        this.isdamaged = isdamaged;
    }

    public int getQuantity_updated() {
        return quantity_updated;
    }

    public void setQuantity_updated(int quantity_updated) {
        this.quantity_updated = quantity_updated;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getSalesDetailsId() {
        return salesDetailsId;
    }

    public void setSalesDetailsId(String salesDetailsId) {
        this.salesDetailsId = salesDetailsId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getShopid() {
        return shopid;
    }

    public void setShopid(String shopid) {
        this.shopid = shopid;
    }

    public String getStockid() {
        return stockid;
    }

    public void setStockid(String stockid) {
        this.stockid = stockid;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQtyToDeliver() {
        return qtyToDeliver;
    }

    public void setQtyToDeliver(String qtyToDeliver) {
        this.qtyToDeliver = qtyToDeliver;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(String deliverydate) {
        this.deliverydate = deliverydate;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public Object getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Object returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnStatus() {
        return returnStatus;
    }

    public void setReturnStatus(String returnStatus) {
        this.returnStatus = returnStatus;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCashBack() {
        return cashBack;
    }

    public void setCashBack(String cashBack) {
        this.cashBack = cashBack;
    }

    public Object getFeedback() {
        return feedback;
    }

    public void setFeedback(Object feedback) {
        this.feedback = feedback;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

    public String getCartid() {
        return cartid;
    }

    public void setCartid(String cartid) {
        this.cartid = cartid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getSubCatid() {
        return subCatid;
    }

    public void setSubCatid(String subCatid) {
        this.subCatid = subCatid;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsreturnable() {
        return isreturnable;
    }

    public void setIsreturnable(String isreturnable) {
        this.isreturnable = isreturnable;
    }

    public Object getMake() {
        return make;
    }

    public void setMake(Object make) {
        this.make = make;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getWeight() {
        return weight;
    }

    public void setWeight(Object weight) {
        this.weight = weight;
    }

    public Object getSize() {
        return size;
    }

    public void setSize(Object size) {
        this.size = size;
    }

    public Object getThickness() {
        return thickness;
    }

    public void setThickness(Object thickness) {
        this.thickness = thickness;
    }

    public Object getColor() {
        return color;
    }

    public void setColor(Object color) {
        this.color = color;
    }

    public Object getSizedifference() {
        return sizedifference;
    }

    public void setSizedifference(Object sizedifference) {
        this.sizedifference = sizedifference;
    }

    public Object getImage1() {
        return image1;
    }

    public void setImage1(Object image1) {
        this.image1 = image1;
    }

    public Object getImage2() {
        return image2;
    }

    public void setImage2(Object image2) {
        this.image2 = image2;
    }

    public Object getImage3() {
        return image3;
    }

    public void setImage3(Object image3) {
        this.image3 = image3;
    }

    public Object getImage4() {
        return image4;
    }

    public void setImage4(Object image4) {
        this.image4 = image4;
    }

    public Object getImage5() {
        return image5;
    }

    public void setImage5(Object image5) {
        this.image5 = image5;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getDistributorId() {
        return distributorId;
    }

    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }

    public String getMakeDate() {
        return makeDate;
    }

    public void setMakeDate(String makeDate) {
        this.makeDate = makeDate;
    }

    public String getExpeDate() {
        return expeDate;
    }

    public void setExpeDate(String expeDate) {
        this.expeDate = expeDate;
    }

    public String getQtyStock() {
        return qtyStock;
    }

    public void setQtyStock(String qtyStock) {
        this.qtyStock = qtyStock;
    }

    public String getQtyDamage() {
        return qtyDamage;
    }

    public void setQtyDamage(String qtyDamage) {
        this.qtyDamage = qtyDamage;
    }

    public String getQtyRol() {
        return qtyRol;
    }

    public void setQtyRol(String qtyRol) {
        this.qtyRol = qtyRol;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getIsOffer() {
        return isOffer;
    }

    public void setIsOffer(String isOffer) {
        this.isOffer = isOffer;
    }

    public String getOfferPercent() {
        return offerPercent;
    }

    public void setOfferPercent(String offerPercent) {
        this.offerPercent = offerPercent;
    }

    public String getIsCashBack() {
        return isCashBack;
    }

    public void setIsCashBack(String isCashBack) {
        this.isCashBack = isCashBack;
    }

    public String getCashbackAmount() {
        return cashbackAmount;
    }

    public void setCashbackAmount(String cashbackAmount) {
        this.cashbackAmount = cashbackAmount;
    }

    public String getTotalWithGst() {
        return totalWithGst;
    }

    public void setTotalWithGst(String totalWithGst) {
        this.totalWithGst = totalWithGst;
    }

    public String getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getIswide() {
        return iswide;
    }

    public void setIswide(String iswide) {
        this.iswide = iswide;
    }

    public String getStockUpdateDate() {
        return stockUpdateDate;
    }

    public void setStockUpdateDate(String stockUpdateDate) {
        this.stockUpdateDate = stockUpdateDate;
    }

    public Object getCommitionAgentId() {
        return commitionAgentId;
    }

    public void setCommitionAgentId(Object commitionAgentId) {
        this.commitionAgentId = commitionAgentId;
    }

    public String getAddedUserId() {
        return addedUserId;
    }

    public void setAddedUserId(String addedUserId) {
        this.addedUserId = addedUserId;
    }

    public String getBarcodeNo() {
        return barcodeNo;
    }

    public void setBarcodeNo(String barcodeNo) {
        this.barcodeNo = barcodeNo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPurchaseWithoutGst() {
        return purchaseWithoutGst;
    }

    public void setPurchaseWithoutGst(String purchaseWithoutGst) {
        this.purchaseWithoutGst = purchaseWithoutGst;
    }

    public String getPurchaseWithTax() {
        return purchaseWithTax;
    }

    public void setPurchaseWithTax(String purchaseWithTax) {
        this.purchaseWithTax = purchaseWithTax;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getCashbackPercent() {
        return cashbackPercent;
    }

    public void setCashbackPercent(String cashbackPercent) {
        this.cashbackPercent = cashbackPercent;
    }

    public String getSalespriWithoutGst() {
        return salespriWithoutGst;
    }

    public void setSalespriWithoutGst(String salespriWithoutGst) {
        this.salespriWithoutGst = salespriWithoutGst;
    }

    public String getSalespriWithGst() {
        return salespriWithGst;
    }

    public void setSalespriWithGst(String salespriWithGst) {
        this.salespriWithGst = salespriWithGst;
    }

    public String getReffIncomePercent() {
        return reffIncomePercent;
    }

    public void setReffIncomePercent(String reffIncomePercent) {
        this.reffIncomePercent = reffIncomePercent;
    }

    public String getReffGenStatus() {
        return reffGenStatus;
    }

    public void setReffGenStatus(String reffGenStatus) {
        this.reffGenStatus = reffGenStatus;
    }

    public String getCashbackGenStatus() {
        return cashbackGenStatus;
    }

    public void setCashbackGenStatus(String cashbackGenStatus) {
        this.cashbackGenStatus = cashbackGenStatus;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReferedUserid() {
        return referedUserid;
    }

    public void setReferedUserid(String referedUserid) {
        this.referedUserid = referedUserid;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
