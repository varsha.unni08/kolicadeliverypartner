package com.centroid.kolica.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.centroid.kolica.R;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.domain.ProductOrderData;

import java.util.ArrayList;
import java.util.List;

public class ProductOrderAdapter extends RecyclerView.Adapter<ProductOrderAdapter.ProductOrderHolder> {


    Context context;
    List<ProductOrderData>orderData;
    int code;

    List<ProductOrderData>orderData_selected=new ArrayList<>();

    public ProductOrderAdapter(Context context, List<ProductOrderData> orderData,int code) {
        this.context = context;
        this.orderData = orderData;
        this.code=code;
    }

    public class ProductOrderHolder extends RecyclerView.ViewHolder{

        ImageView imgproduct;
        TextView txtProductname,txtunitprice,txtQty,txtTotal;
        CheckBox cb_checked;

        public ProductOrderHolder(@NonNull View itemView) {
            super(itemView);

            imgproduct=itemView.findViewById(R.id.imgproduct);
            txtProductname=itemView.findViewById(R.id.txtProductname);
            txtunitprice=itemView.findViewById(R.id.txtunitprice);
            txtQty=itemView.findViewById(R.id.txtQty);
            txtTotal=itemView.findViewById(R.id.txtTotal);
            cb_checked=itemView.findViewById(R.id.cb_checked);

            cb_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b)
                    {

                        addSelectedProductOrders(orderData.get(getAdapterPosition()));

                    }
                    else{

                        removeSelectedProductOrders(orderData.get(getAdapterPosition()));
                    }
                }
            });

        }
    }


    @NonNull
    @Override
    public ProductOrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_productdata,parent,false);


        return new ProductOrderHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductOrderHolder holder, int position) {

        if(code==1)
        {holder.cb_checked.setVisibility(View.VISIBLE);
        }

        holder.txtProductname.setText(orderData.get(position).getProductName());
        holder.txtunitprice.setText(orderData.get(position).getSalespriWithGst()+" ₹");

        holder.txtQty.setText(orderData.get(position).getQty());

        holder.txtTotal.setText(orderData.get(position).getTotalPrice()+" ₹");


        if(orderData.get(position).getImage1()!=null||!orderData.get(position).getImage1().toString().isEmpty()) {

            Glide.with(context).load(ServerData.imageurl+orderData.get(position).getImage1().toString()).into(holder.imgproduct);
        }
        else{

//            Glide.with(context).load(orderData.get(position).getImage1().toString()).into(holder.imgproduct);

        }

    }

    @Override
    public int getItemCount() {
        return orderData.size();
    }


    public List<ProductOrderData> getSelectedProductOrders()
    {

        return orderData_selected;
    }

    public void addSelectedProductOrders(ProductOrderData productOrderData)
    {

         orderData_selected.add(productOrderData);
    }


    public void removeSelectedProductOrders(ProductOrderData productOrderData)
    {

        for( int i=0;i<orderData_selected.size();i++)
        {
            ProductOrderData pr=orderData_selected.get(i);
            if(pr.getSalesDetailsId().equalsIgnoreCase(productOrderData.getSalesDetailsId()))
            {

                orderData_selected.remove(i);
            }


        }


    }
}
