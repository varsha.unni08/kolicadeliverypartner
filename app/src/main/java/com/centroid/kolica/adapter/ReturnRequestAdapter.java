package com.centroid.kolica.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.centroid.kolica.R;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.domain.ReturnRequestData;

import java.util.List;

public class ReturnRequestAdapter extends RecyclerView.Adapter<ReturnRequestAdapter.ReturnRequestHolder> {

    Context context;
    List<ReturnRequestData>rqd;

    public ReturnRequestAdapter(Context context, List<ReturnRequestData> rqd) {
        this.context = context;
        this.rqd = rqd;
    }

    public class ReturnRequestHolder extends RecyclerView.ViewHolder{

        ImageView imgproduct;
        TextView txtProductname,txtunitprice,txtQty,txtTotal;

        public ReturnRequestHolder(@NonNull View itemView) {
            super(itemView);
            txtTotal=itemView.findViewById(R.id.txtTotal);
            txtQty=itemView.findViewById(R.id.txtQty);
            txtunitprice=itemView.findViewById(R.id.txtunitprice);
            txtProductname=itemView.findViewById(R.id.txtProductname);
            imgproduct=itemView.findViewById(R.id.imgproduct);
        }
    }


    @NonNull
    @Override
    public ReturnRequestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_requestadapter,parent,false);


        return new ReturnRequestHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReturnRequestHolder holder, int position) {

        double d=Double.parseDouble(rqd.get(position).getSalespriWithGst());
        int q=Integer.parseInt(rqd.get(position).getQty());

        double total=d*q;

        holder.txtProductname.setText("Name : "+rqd.get(position).getProductName());
        holder.txtTotal.setText("Total price : "+total+" ₹");
        holder.txtQty.setText("Quantity : "+rqd.get(position).getQty());
        holder.txtunitprice.setText("Unit price : "+rqd.get(position).getSalespriWithGst()+" ₹");

        if(rqd.get(position).getImage1()!=null||!rqd.get(position).getImage1().toString().isEmpty()) {

            Glide.with(context).load(ServerData.imageurl+rqd.get(position).getImage1().toString()).into(holder.imgproduct);
        }
        else{

//            Glide.with(context).load(orderData.get(position).getImage1().toString()).into(holder.imgproduct);

        }

    }

    @Override
    public int getItemCount() {
        return rqd.size();
    }
}
