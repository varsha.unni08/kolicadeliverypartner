package com.centroid.kolica.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.kolica.R;
import com.centroid.kolica.domain.ProductOrderData;

import java.util.List;

public class BillOrderAdapter extends RecyclerView.Adapter<BillOrderAdapter.BillHolder> {

    Context context;
    List<ProductOrderData>prd;

    public BillOrderAdapter(Context context, List<ProductOrderData> prd) {
        this.context = context;
        this.prd = prd;
    }

    @NonNull
    @Override
    public BillHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_bill,parent,false);


        return new BillHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BillHolder holder, int position) {

        holder.txtname.setText(prd.get(position).getProductName());

        holder.txtunitprice.setText(prd.get(position).getSalespriWithGst()+" ₹");

        holder.txtqty.setText(prd.get(position).getQuantity_updated()+"");

        double p=Double.parseDouble(prd.get(position).getSalespriWithGst());

        double totalprice=p*prd.get(position).getQuantity_updated();

        holder.txtTotal.setText(totalprice+" ₹");

    }

    @Override
    public int getItemCount() {
        return prd.size();
    }

    public class BillHolder extends RecyclerView.ViewHolder
    {

        TextView txtname,txtunitprice,txtqty,txtTotal;

        public BillHolder(@NonNull View itemView) {
            super(itemView);

            txtname=itemView.findViewById(R.id.txtname);
            txtunitprice=itemView.findViewById(R.id.txtunitprice);
            txtqty=itemView.findViewById(R.id.txtqty);
            txtTotal=itemView.findViewById(R.id.txtTotal);
        }
    }
}
