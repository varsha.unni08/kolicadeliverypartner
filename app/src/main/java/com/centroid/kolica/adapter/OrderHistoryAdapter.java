package com.centroid.kolica.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.centroid.kolica.R;
import com.centroid.kolica.domain.OrderData;

import java.util.List;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.OrderHolder> {

    List<OrderData> orderData;
    Context context;

    public OrderHistoryAdapter(List<OrderData> ord1, Context context) {
        this.orderData = ord1;
        this.context = context;
    }

    public class OrderHolder extends RecyclerView.ViewHolder{
        TextView txtorderid,txtorderdate,txttotal,txttype,txtStatus,txtdeliverycharge;

        public OrderHolder(@NonNull View itemView) {
            super(itemView);
            txttype=itemView.findViewById(R.id.txttype);
            txttotal=itemView.findViewById(R.id.txttotal);
            txtorderdate=itemView.findViewById(R.id.txtorderdate);
            txtorderid=itemView.findViewById(R.id.txtorderid);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            txtdeliverycharge=itemView.findViewById(R.id.txtdeliverycharge);
        }
    }

    @NonNull
    @Override
    public OrderHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_orderadapter,parent,false);



        return new OrderHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderHolder holder, int position) {
        holder.txtorderid.setText(orderData.get(position).getId());
        holder.txtorderdate.setText(orderData.get(position).getOrderDate());
        holder.txttotal.setText(orderData.get(position).getTotalPrice()+" ₹");
        holder.txtdeliverycharge.setText(orderData.get(position).getDeleveryCharge()+" ₹");

        if(orderData.get(position).getPaymentMethode().equalsIgnoreCase("1")) {
            holder.txttype.setText("Wallet");
        }
        else if(orderData.get(position).getPaymentMethode().equalsIgnoreCase("2")) {

            holder.txttype.setText("Cash on delivery");
        }
        else{

            holder.txttype.setText("Online");
        }


        if(orderData.get(position).getCancelStatus().equalsIgnoreCase("1"))
        {

            holder.txtStatus.setText("Cancelled");
            holder.txtStatus.setTextColor(Color.RED);
        }
        else if(orderData.get(position).getDeliveryCompletionStatus().equalsIgnoreCase("1"))
        {

            holder.txtStatus.setText("Delivered");
            holder.txtStatus.setTextColor(Color.GREEN);
        }
        else{

            holder.txtStatus.setText("Pending....");
            holder.txtStatus.setTextColor(Color.MAGENTA);

        }
    }

    @Override
    public int getItemCount() {
        return orderData.size();
    }
}
