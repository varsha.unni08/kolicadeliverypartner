package com.centroid.kolica.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.centroid.kolica.R;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.domain.ProductOrderData;

import java.util.ArrayList;
import java.util.List;

public class ReturnProductAdapter extends RecyclerView.Adapter<ReturnProductAdapter.ReturnProductHolder> {


    Context context;
    List<ProductOrderData>orderData;
    List<ProductOrderData>orderData_selected=new ArrayList<>();
    public ReturnProductAdapter(Context context, List<ProductOrderData> productOrderData) {
        this.context = context;
        this.orderData = productOrderData;
    }

    public class ReturnProductHolder extends RecyclerView.ViewHolder{

        ImageView imgproduct;
        TextView txtProductname,txtunitprice,txtTotal,txtQty,txtReturnedqty;
        CheckBox cb_checked,cb_damaged;
        EditText edtQty;

        public ReturnProductHolder(@NonNull View itemView) {
            super(itemView);

            imgproduct=itemView.findViewById(R.id.imgproduct);
            txtProductname=itemView.findViewById(R.id.txtProductname);
            txtunitprice=itemView.findViewById(R.id.txtunitprice);
            edtQty=itemView.findViewById(R.id.edtQty);
            txtTotal=itemView.findViewById(R.id.txtTotal);
            cb_checked=itemView.findViewById(R.id.cb_checked);
            txtQty=itemView.findViewById(R.id.txtQty);
            txtReturnedqty=itemView.findViewById(R.id.txtReturnedqty);
            cb_damaged=itemView.findViewById(R.id.cb_damaged);
            cb_damaged.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    if(b)
                    {
                        orderData.get(getAdapterPosition()).setIsdamaged(1);
                    }
                    else{

                        orderData.get(getAdapterPosition()).setIsdamaged(0);
                    }

                    notifyDataSetChanged();

                }
            });

            edtQty.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(!charSequence.toString().equalsIgnoreCase(""))
                    {



                            int enteredqty = Integer.parseInt(charSequence.toString());

                           // int orderedqty = Integer.parseInt(orderData.get(getAdapterPosition()).getQty());



                                orderData.get(getAdapterPosition()).setQuantity_updated(enteredqty);




                         //   notifyDataSetChanged();




                    }
                    else {

                            orderData.get(getAdapterPosition()).setQuantity_updated(0);
                           // notifyDataSetChanged();

                    }


                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            cb_checked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b)
                    {

                        orderData.get(getAdapterPosition()).setSelected(1);


                        addSelectedProductOrders(orderData.get(getAdapterPosition()));

                    }
                    else{
                        orderData.get(getAdapterPosition()).setSelected(0);

                        removeSelectedProductOrders(orderData.get(getAdapterPosition()));


                    }

                    notifyDataSetChanged();
                }
            });
        }
    }

    @NonNull
    @Override
    public ReturnProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_returnproductadapter,parent,false);


        return new ReturnProductHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ReturnProductHolder holder, int position) {



           // holder.cb_checked.setVisibility(View.VISIBLE);


        holder.txtProductname.setText(orderData.get(position).getProductName());
        holder.txtunitprice.setText(orderData.get(position).getSalespriWithGst()+" ₹");

        if(orderData.get(position).getIsdamaged()==0)
        {
            holder.cb_damaged.setChecked(false);

        }
        else{

            holder.cb_damaged.setChecked(true);

        }



        if(orderData.get(position).getSelected()==1)
        {
            holder.edtQty.setVisibility(View.VISIBLE);
           // holder.txtQty.setVisibility(View.GONE);
            holder.txtReturnedqty.setVisibility(View.VISIBLE);
            holder.edtQty.setText(orderData.get(position).getQuantity_updated()+"");
            holder.cb_checked.setChecked(true);
            holder.cb_damaged.setVisibility(View.VISIBLE);
        }
        else{
            holder.cb_damaged.setVisibility(View.GONE);
            holder.edtQty.setVisibility(View.GONE);
          //  holder.txtQty.setVisibility(View.VISIBLE);
            holder.txtReturnedqty.setVisibility(View.GONE);
            orderData.get(position).setQuantity_updated(Integer.parseInt(orderData.get(position).getQty()));
            holder.cb_checked.setChecked(false);

            holder.edtQty.setText(orderData.get(position).getQuantity_updated()+"");
        }

//        if(orderData.get(position).getQuantity_updated()==-1)
//        {
//
//            holder.txtReturnedqty.setText("Your entered quantity is greater than ordered quantity");
//
//        }
//        else if(orderData.get(position).getQuantity_updated()>0)
//        {
//
//            holder.txtReturnedqty.setText("Quantity to return : "+orderData.get(position).getQuantity_updated());
//        }
//        else{
//
//            holder.txtReturnedqty.setText("Customer needs to return full quantity");
//
//        }


        holder.txtQty.setText(orderData.get(position).getQty());

        holder.txtTotal.setText(orderData.get(position).getTotalPrice()+" ₹");


        if(orderData.get(position).getImage1()!=null||!orderData.get(position).getImage1().toString().isEmpty()) {

            Glide.with(context).load(ServerData.imageurl+orderData.get(position).getImage1().toString()).into(holder.imgproduct);
        }
        else{

//            Glide.with(context).load(orderData.get(position).getImage1().toString()).into(holder.imgproduct);

        }

    }

    @Override
    public int getItemCount() {
        return orderData.size();
    }

    public List<ProductOrderData> getSelectedProductOrders()
    {

        return orderData_selected;
    }

    public List<ProductOrderData> getAllProductOrders()
    {

        return orderData;
    }

    public void addSelectedProductOrders(ProductOrderData productOrderData)
    {

        orderData_selected.add(productOrderData);
    }


    public void removeSelectedProductOrders(ProductOrderData productOrderData)
    {

        for( int i=0;i<orderData_selected.size();i++)
        {
            ProductOrderData pr=orderData_selected.get(i);
            if(pr.getSalesDetailsId().equalsIgnoreCase(productOrderData.getSalesDetailsId()))
            {

                orderData_selected.remove(i);
            }


        }


    }
}
