package com.centroid.kolica.constants;

public class ServerData {

   public  static String baseurl="https://centroidsolutions.in/Kolica/KolicaApi/";

   public static String imageurl="https://centroidsolutions.in/Kolica/KolicaAdmin/images/productimages/products/";



    public static String getSalesOrder="DeliveryPartnerApi/getOrders";

    public static String getSalesOrderItems="DeliveryPartnerApi/getOrderDetails";

    public static String login="DeliveryPartnerApi/getUserDetails";
    public static String getAddressById="DeliveryPartnerApi/getAddressById";

    public static String updateDeliveryStatus="DeliveryPartnerApi/updateDeliveryStatus";

    public static String getOrderhistory="DeliveryPartnerApi/getOrderhistory";
    public static String getReturnRequests="DeliveryPartnerApi/getReturnRequests";
    public static String getSettingsData="DeliveryPartnerApi/getSettingsData";
    public static String updateReturnItems="DeliveryPartnerApi/updateReturnItems";


    public static String userkey="userkey";

    public static String mobile="mobile";
}
