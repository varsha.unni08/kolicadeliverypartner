package com.centroid.kolica.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.centroid.kolica.R;
import com.centroid.kolica.adapter.OrderDataAdapter;
import com.centroid.kolica.adapter.OrderHistoryAdapter;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.domain.OrderData;
import com.centroid.kolica.domain.Orders;
import com.centroid.kolica.preferencehelper.PreferenceHelper;
import com.centroid.kolica.serverHandler.RequestHandler;
import com.centroid.kolica.serverHandler.ResponseHandler;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryActivity extends AppCompatActivity {

    ImageView imgback;

    RecyclerView recycler;

    SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        recycler=findViewById(R.id.recycler);

        swipeContainer=findViewById(R.id.swipeContainer);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrders();
            }
        });



        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        getOrders();

    }

    public void getOrders()
    {
        swipeContainer.setRefreshing(true);

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",new Date().toString());


        new RequestHandler(HistoryActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                swipeContainer.setRefreshing(false);
                // progressFragment.dismiss();

                Log.e("LoginResponse",data);

                if(data!=null)
                {

                    Orders ord= new GsonBuilder().create().fromJson(data,Orders.class);

                    if(ord.getStatus()==1)
                    {

                        List<OrderData> ord1=new ArrayList<>();

                        ord1.addAll(ord.getData());

                        Collections.reverse(ord1);


                        recycler.setLayoutManager(new LinearLayoutManager(HistoryActivity.this));
                        recycler.setAdapter(new OrderHistoryAdapter(ord1,HistoryActivity.this));



                    }
                    else{



                    }








                }

            }

            @Override
            public void onFailure(String err) {
                //  progressFragment.dismiss();
                swipeContainer.setRefreshing(false);
                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, ServerData.getOrderhistory+"/"+new PreferenceHelper(HistoryActivity.this).getData(ServerData.mobile), Request.Method.GET).submitRequest();


    }
}