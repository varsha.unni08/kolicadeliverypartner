package com.centroid.kolica.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.android.volley.Request;
import com.centroid.kolica.R;
import com.centroid.kolica.adapter.OrderDataAdapter;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.domain.OrderData;
import com.centroid.kolica.domain.Orders;
import com.centroid.kolica.preferencehelper.PreferenceHelper;
import com.centroid.kolica.progress.ProgressFragment;
import com.centroid.kolica.serverHandler.RequestHandler;
import com.centroid.kolica.serverHandler.ResponseHandler;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrdersActivity extends AppCompatActivity {

    ImageView imgback;

     SwipeRefreshLayout swipeContainer;

     RecyclerView rvItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        rvItems=findViewById(R.id.rvItems);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(OrdersActivity.this, view);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.returnrequest:
                               // archive(item);

                                startActivity(new Intent(OrdersActivity.this,ReturnRequestActivity.class));


                                return true;
                            case R.id.history:
                               // delete(item);

                                startActivity(new Intent(OrdersActivity.this,HistoryActivity.class));

                                return true;

                            case R.id.share:
                                // delete(item);
                                new  AlertDialog.Builder(OrdersActivity.this)
                                        .setTitle(R.string.app_name)
                                        .setMessage("Do you want to logout now  ?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                new PreferenceHelper(OrdersActivity.this).putData(ServerData.userkey,"");
                                                dialog.dismiss();
                                                System.exit(0);


                                                // Whatever...
                                            }
                                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        dialogInterface.dismiss();
                                    }
                                }).show();


                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.inflate(R.menu.menu_popup);
                popup.show();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrderDetails();
            }
        });



        getOrderDetails();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        getOrderDetails();
    }

    public void getOrderDetails()
    {
//        ProgressFragment progressFragment=new ProgressFragment();
//        progressFragment.show(getSupportFragmentManager(),"sdjfn");

swipeContainer.setRefreshing(true);

        Map<String,String> params=new HashMap<>();
        params.put("timestamp",new Date().toString());


        new RequestHandler(OrdersActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                swipeContainer.setRefreshing(false);
               // progressFragment.dismiss();

                Log.e("LoginResponse",data);

                if(data!=null)
                {

                    Orders ord= new GsonBuilder().create().fromJson(data,Orders.class);

                    if(ord.getStatus()==1)
                    {

                        List<OrderData>ord1=new ArrayList<>();

                        ord1.addAll(ord.getData());

                        Collections.reverse(ord1);


                        rvItems.setLayoutManager(new LinearLayoutManager(OrdersActivity.this));
                        rvItems.setAdapter(new OrderDataAdapter(OrdersActivity.this,ord1));



                    }
                    else{



                    }








                }

            }

            @Override
            public void onFailure(String err) {
              //  progressFragment.dismiss();
                swipeContainer.setRefreshing(false);
                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, ServerData.getSalesOrder+"/"+new PreferenceHelper(OrdersActivity.this).getData(ServerData.mobile), Request.Method.GET).submitRequest();



    }
}