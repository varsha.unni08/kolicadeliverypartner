package com.centroid.kolica.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.centroid.kolica.R;
import com.centroid.kolica.UserAlerts;
import com.centroid.kolica.adapter.BillOrderAdapter;
import com.centroid.kolica.adapter.OrderDataAdapter;
import com.centroid.kolica.adapter.ProductOrderAdapter;
import com.centroid.kolica.adapter.ReturnProductAdapter;
import com.centroid.kolica.adapter.ReturnRequestAdapter;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.domain.KolicaSettings;
import com.centroid.kolica.domain.OrderData;
import com.centroid.kolica.domain.Orders;
import com.centroid.kolica.domain.ProductOrder;
import com.centroid.kolica.domain.ProductOrderData;
import com.centroid.kolica.domain.ReturnRequest;
import com.centroid.kolica.domain.ReturnRequestData;
import com.centroid.kolica.domain.UserAddress;
import com.centroid.kolica.preferencehelper.PreferenceHelper;
import com.centroid.kolica.progress.ProgressFragment;
import com.centroid.kolica.serverHandler.RequestHandler;
import com.centroid.kolica.serverHandler.ResponseHandler;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OrderDetailsActivity extends AppCompatActivity {
    Intent intent;
    int code = 0;
    OrderData orderData;

    CardView cardlocation, card_product, card_returnproduct;

    String latitude = "0";
    String longitude = "0";
    ImageView imglocation, imgproducts, imgretunproducts;

    List<ProductOrderData> prdata;

    ProductOrder prdo = null;
    List<ProductOrderData> pr_readytoUpdate = new ArrayList<>();
    Button btncollectAmount, btndeliverycomplete,btnReturnproduct;
    RadioButton rbNo,rbYes;

    TextView txtreturnProducts, txtProducts, txtlocation, txtorderid, txtorderdate, txttotal, txttype, txtStatus, txtPhone, txtUsername, txtAddress, txtdeliverycharge;
    double t = 0,deliverychargeamount=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        getSupportActionBar().hide();
        intent = getIntent();
        orderData = (OrderData) intent.getSerializableExtra("OrderDetails");
        prdata = new ArrayList<>();
        card_returnproduct = findViewById(R.id.card_returnproduct);
        imgretunproducts = findViewById(R.id.imgretunproducts);
        txtreturnProducts = findViewById(R.id.txtreturnProducts);
        btnReturnproduct=findViewById(R.id.btnReturnproduct);

        rbYes=findViewById(R.id.rbYes);
        rbNo=findViewById(R.id.rbNo);

        rbYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    btnReturnproduct.setVisibility(View.VISIBLE);
                    btncollectAmount.setVisibility(View.GONE);

                }
                else{

                    btnReturnproduct.setVisibility(View.GONE);
                    btncollectAmount.setVisibility(View.VISIBLE);
                }
            }
        });


        rbNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    btnReturnproduct.setVisibility(View.GONE);
                    btncollectAmount.setVisibility(View.VISIBLE);

                }
                else{

                    btnReturnproduct.setVisibility(View.VISIBLE);
                    btncollectAmount.setVisibility(View.GONE);
                }
            }
        });

        btnReturnproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReturnProductList();
            }
        });




        card_returnproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReturnProductList();
            }
        });

        imgretunproducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReturnProductList();
            }
        });

        txtreturnProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReturnProductList();
            }
        });


        txttype = findViewById(R.id.txttype);
        txttotal = findViewById(R.id.txttotal);
        txtorderdate = findViewById(R.id.txtorderdate);
        txtorderid = findViewById(R.id.txtorderid);
        txtStatus = findViewById(R.id.txtStatus);

        btncollectAmount = findViewById(R.id.btncollectAmount);

        btndeliverycomplete = findViewById(R.id.btndeliverycomplete);

        txtUsername = findViewById(R.id.txtUsername);
        txtPhone = findViewById(R.id.txtPhone);

        txtAddress = findViewById(R.id.txtAddress);
        cardlocation = findViewById(R.id.cardlocation);
        txtdeliverycharge = findViewById(R.id.txtdeliverycharge);
        imglocation = findViewById(R.id.imglocation);
        txtlocation = findViewById(R.id.txtlocation);

        card_product = findViewById(R.id.card_product);
        txtProducts = findViewById(R.id.txtProducts);
        imgproducts = findViewById(R.id.imgproducts);

        txtorderid.setText(orderData.getId());
        txtorderdate.setText(orderData.getOrderDate());
        txttotal.setText(orderData.getTotalPrice() + " ₹");
        txtdeliverycharge.setText(orderData.getDeleveryCharge() + " ₹");

        if (orderData.getPaymentMethode().equalsIgnoreCase("2")) {

            btncollectAmount.setVisibility(View.VISIBLE);
            btndeliverycomplete.setVisibility(View.GONE);
        } else {
            btncollectAmount.setVisibility(View.GONE);
            btndeliverycomplete.setVisibility(View.VISIBLE);

        }

        if (orderData.getCancelStatus().equalsIgnoreCase("1")) {

            btncollectAmount.setVisibility(View.GONE);
            btndeliverycomplete.setVisibility(View.GONE);
        } else if (orderData.getDeliveryCompletionStatus().equalsIgnoreCase("1")) {

            btncollectAmount.setVisibility(View.GONE);
            btndeliverycomplete.setVisibility(View.GONE);
        }


//        btndeliverycomplete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                updateDeliveryStatus();
//
//            }
//        });

        btncollectAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double totalprice = Double.parseDouble(orderData.getTotalPrice());

                double deliverycharge = Double.parseDouble(orderData.getDeleveryCharge());

                showAmountDialog(totalprice,deliverycharge,0);

            }
        });


        if (orderData.getPaymentMethode().equalsIgnoreCase("1")) {
            txttype.setText("Wallet");
        } else if (orderData.getPaymentMethode().equalsIgnoreCase("2")) {

            txttype.setText("Cash on delivery");
        } else {

            txttype.setText("Online");
        }


        if (orderData.getCancelStatus().equalsIgnoreCase("1")) {

            txtStatus.setText("Cancelled");
            txtStatus.setTextColor(Color.RED);
        } else if (orderData.getDeliveryCompletionStatus().equalsIgnoreCase("1")) {

            txtStatus.setText("Delivered");
            txtStatus.setTextColor(Color.GREEN);
        } else {

            txtStatus.setText("Pending....");
            txtStatus.setTextColor(Color.MAGENTA);

        }

        getOrderDetails();
        getAddressDetails();

        imglocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!latitude.isEmpty() && !longitude.isEmpty()) {

                    if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")) {

                        String url = "https://maps.google.com?q=" + latitude + "," + longitude;

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);

                    } else {

                        UserAlerts.showAlert("Cannot fetch location", OrderDetailsActivity.this);
                    }

                } else {

                    UserAlerts.showAlert("Cannot fetch location", OrderDetailsActivity.this);
                }
            }
        });

        txtlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!latitude.isEmpty() && !longitude.isEmpty()) {

                    if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")) {

                        String url = "https://maps.google.com?q=" + latitude + "," + longitude;

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);

                    } else {

                        UserAlerts.showAlert("Cannot fetch location", OrderDetailsActivity.this);
                    }

                } else {

                    UserAlerts.showAlert("Cannot fetch location", OrderDetailsActivity.this);
                }
            }
        });

        cardlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!latitude.isEmpty() && !longitude.isEmpty()) {

                    if (!latitude.equalsIgnoreCase("0") && !longitude.equalsIgnoreCase("0")) {

                        String url = "https://maps.google.com?q=" + latitude + "," + longitude;

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);

                    } else {

                        UserAlerts.showAlert("Cannot fetch location", OrderDetailsActivity.this);
                    }

                } else {

                    UserAlerts.showAlert("Cannot fetch location", OrderDetailsActivity.this);
                }


            }
        });


        card_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showProductList();
            }
        });

        txtProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProductList();
            }
        });

        imgproducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProductList();
            }
        });


    }


    public void waitForUpdate()
    {
        ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "sdjfn");
        Handler h=new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressFragment.dismiss();
                onBackPressed();

            }
        },12*1000);

    }

    public void updateDeliveryStatus(int upi) {

        ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "sdjfn");

        Map<String, String> params = new HashMap<>();
        params.put("timestamp", new Date().toString());


        new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //  swipeContainer.setRefreshing(false);
                progressFragment.dismiss();

                Log.e("LoginResponse", data);

                if (data != null) {

                    try {
                        JSONObject jsonObject = new JSONObject(data);

                        if (jsonObject.getInt("status") == 1) {

                            orderData.setDeliveryCompletionStatus("1");

                          //  onBackPressed();
                            //getOrderDetails();


                        } else {


                            UserAlerts.showAlert("Error", OrderDetailsActivity.this);

                        }


                    } catch (Exception e) {

                    }


                    // Toast.makeText(OrderDetailsActivity.this,data,Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();
                // swipeContainer.setRefreshing(false);
                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, ServerData.updateDeliveryStatus + "/" + orderData.getId() + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile) + "/" + upi + "", Request.Method.GET).submitRequest();


    }


    public void showAmountDialog(double totalprice,double deliverycharge,int isreturn) {

//        double totalprice = Double.parseDouble(orderData.getTotalPrice());
//
//        double deliverycharge = Double.parseDouble(orderData.getDeleveryCharge());
        double totalamount = totalprice + deliverycharge;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        final Dialog dialog = new Dialog(OrderDetailsActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.layout_amount_confirm);
        dialog.setTitle(R.string.app_name);
        int b = (int) (height / 1.2);
        // dialog.getWindow().setLayout(width,height );


        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        EditText edtPh = dialog.findViewById(R.id.edtPh);
        TextView txtAmount = dialog.findViewById(R.id.txtAmount);

        RadioButton rbByCash = dialog.findViewById(R.id.rbByCash);

        RadioButton rbByUpi = dialog.findViewById(R.id.rbByUpi);

        ImageView imgqr = dialog.findViewById(R.id.imgqr);

        TextView txtwarning = dialog.findViewById(R.id.txtwarning);


        txtAmount.setText(totalamount + " ₹");

        rbByCash.setChecked(true);


        edtPh.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(15, 3)});


        rbByCash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {

                    edtPh.setVisibility(View.VISIBLE);
                    imgqr.setVisibility(View.GONE);
                } else {

                    edtPh.setVisibility(View.GONE);
                    imgqr.setVisibility(View.VISIBLE);
                }

            }
        });

        rbByUpi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    edtPh.setVisibility(View.GONE);
                    imgqr.setVisibility(View.VISIBLE);
                } else {
                    imgqr.setVisibility(View.GONE);
                    edtPh.setVisibility(View.VISIBLE);
                }

            }
        });

        edtPh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                txtwarning.setText("");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (rbByCash.isChecked()) {

                    if (!edtPh.getText().toString().equalsIgnoreCase("")) {

                        double d = Double.parseDouble(edtPh.getText().toString());

                        if (d == totalamount) {

                            dialog.dismiss();

                            if(isreturn==0) {

                                updateDeliveryStatus(0);
                            }
                            else {

                                int j=0;

                                for(int i=0;i<pr_readytoUpdate.size();i++)
                                {

                                    ProductOrderData pdd=pr_readytoUpdate.get(i);

                                    ProgressFragment pg = new ProgressFragment();
                                    pg.show(getSupportFragmentManager(), "");

                                    Map<String, String> params = new HashMap<>();
                                    params.put("timestamp", new Date().toString());
                                    params.put("returnqty", pdd.getQtytobill()+"");
                                    params.put("orderdetailsid", pdd.getSalesDetailsId()+"");
                                    params.put("orderid", pdd.getOrderid()+"");

                                    if(pdd.getIsdamaged()==0) {
                                        params.put("damage", "0");
                                    }
                                    else{
                                        params.put("damage", "1");

                                    }
                                    double tp = Double.parseDouble(pdd.getTotalPrice());
                                    int quantity=Integer.parseInt(pdd.getQty());

                                    double p=tp/quantity;

                                    double totalprice = p * pdd.getQtytobill();

                                    params.put("unitprice", p+"");
                                    params.put("totalpayment", totalprice+"");
                                    params.put("stockid", pdd.getStockid()+"");

                                    new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
                                        @Override
                                        public void onSuccess(String data) {
                                            // swipeContainer.setRefreshing(false);
                                            pg.dismiss();

                                            Log.e("LoginResponse", data);

                                            if (data != null) {

                                                updateDeliveryStatus(0);

                                             //   UserAlerts.showAlert(data,OrderDetailsActivity.this);


                                            }

                                        }

                                        @Override
                                        public void onFailure(String err) {
                                            //  progressFragment.dismiss();
                                            pg.dismiss();
                                            //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                                        }
                                    }, ServerData.updateReturnItems + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile), Request.Method.POST).submitRequest();



                                }


                            }

                            waitForUpdate();

                        } else {

                            txtwarning.setText("Please collect correct amount");

                            // UserAlerts.showAlert("Enter correct amount",OrderDetailsActivity.this);
                        }


                    } else {


                        UserAlerts.showAlert("Enter amount", OrderDetailsActivity.this);
                    }

                } else if (rbByUpi.isChecked()) {

                    new AlertDialog.Builder(OrderDetailsActivity.this)
                            .setTitle(R.string.app_name)
                            .setMessage("Are you sure that customer sent money to our bank account ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialoginterface, int which) {

                                    dialoginterface.dismiss();
                                    dialog.dismiss();

                                    if(isreturn==0) {


                                        updateDeliveryStatus(1);
                                    }else{

                                        int j=0;

                                        for(int i=0;i<pr_readytoUpdate.size();i++)
                                        {

                                            ProductOrderData pdd=pr_readytoUpdate.get(i);

                                            ProgressFragment pg = new ProgressFragment();
                                            pg.show(getSupportFragmentManager(), "");

                                            Map<String, String> params = new HashMap<>();
                                            params.put("timestamp", new Date().toString());
                                            params.put("returnqty", pdd.getQtytobill()+"");
                                            params.put("orderdetailsid", pdd.getSalesDetailsId()+"");
                                            params.put("orderid", pdd.getOrderid()+"");

                                            if(pdd.getIsdamaged()==0) {
                                                params.put("damage", "0");
                                            }
                                            else{
                                                params.put("damage", "1");

                                            }
                                            double p = Double.parseDouble(pdd.getSalespriWithGst());

                                            double totalprice = p * pdd.getQtytobill();

                                            params.put("unitprice", pdd.getSalespriWithGst()+"");
                                            params.put("totalpayment", totalprice+"");
                                            params.put("stockid", pdd.getStockid()+"");

                                            new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
                                                @Override
                                                public void onSuccess(String data) {
                                                    // swipeContainer.setRefreshing(false);
                                                    pg.dismiss();

                                                    Log.e("LoginResponse", data);

                                                    if (data != null) {

                                                        updateDeliveryStatus(1);

                                                     //   UserAlerts.showAlert(data,OrderDetailsActivity.this);


                                                    }

                                                }

                                                @Override
                                                public void onFailure(String err) {
                                                    //  progressFragment.dismiss();
                                                    pg.dismiss();
                                                    //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                                                }
                                            }, ServerData.updateReturnItems + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile), Request.Method.POST).submitRequest();



                                        }

                                    }

                                    waitForUpdate();
                                    // Whatever...
                                }
                            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            dialogInterface.dismiss();
                        }
                    }).show();


                }

            }
        });


        dialog.show();


    }









    public void showReturnProductList() {

        code = 0;

        final Dialog dialog = new Dialog(OrderDetailsActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        View view = getLayoutInflater().inflate(R.layout.layout_prlist_return, null);

        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);
        Button btnPrint = (Button) view.findViewById(R.id.btnPrint);

        Button btnSubmit = (Button) view.findViewById(R.id.btnSubmit);

        if (prdo != null) {

            prdata.clear();
            if (prdo.getStatus() == 1) {
                prdata.addAll(prdo.getData());

            }

            for (ProductOrderData prd : prdata) {
                String s = prd.getQty();
                prd.setQuantity_updated(Integer.parseInt(s));
                prd.setSelected(0);
            }

        }

        ReturnProductAdapter productOrderAdapter = new ReturnProductAdapter(OrderDetailsActivity.this, prdata);

        rv.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this));
        rv.setAdapter(productOrderAdapter);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (code != 0) {

                    List<ProductOrderData> prd = productOrderAdapter.getSelectedProductOrders();

                    Log.d("ProductOrder", prd.size() + "");

                    if (prd.size() > 0) {




                        for (ProductOrderData pr : productOrderAdapter.getAllProductOrders()
                        ) {

                            if (pr.getSelected() == 1) {

                                int qty = Integer.parseInt(pr.getQty());

                                int qty_updated = pr.getQuantity_updated();

                                if(pr.getQuantity_updated()>qty)
                                {
                                    code = 0;
                                    pr_readytoUpdate.clear();
                                    UserAlerts.showAlert("Updated quantity must be less than of ordered quantity in " + pr.getProductName(), OrderDetailsActivity.this);
                                    break;

                                }

                                if(pr.getQuantity_updated()<qty)
                                {

                                    int q=qty-pr.getQuantity_updated();
                                    pr.setQtytobill(q);

                                    pr_readytoUpdate.add(pr);


                                }
                                else{

                                    int q=qty;

                                    pr.setQtytobill(q);
                                    pr_readytoUpdate.add(pr);
                                }


                            }


                        }

//                        for(ProductOrderData pdd:pr_readytoUpdate)
//                        {
//
//                            ProgressFragment pg = new ProgressFragment();
//                            pg.show(getSupportFragmentManager(), "");
//
//                            Map<String, String> params = new HashMap<>();
//                            params.put("timestamp", new Date().toString());
//                            params.put("returnqty", pdd.getQtytobill()+"");
//                            params.put("orderdetailsid", pdd.getSalesDetailsId()+"");
//                            params.put("orderid", pdd.getOrderid()+"");
//
//                            if(pdd.getIsdamaged()==0) {
//                                params.put("damage", "0");
//                            }
//                            else{
//                                params.put("damage", "1");
//
//                            }
//                            double p = Double.parseDouble(pdd.getSalespriWithGst());
//
//                            double totalprice = p * pdd.getQtytobill();
//
//                            params.put("unitprice", pdd.getSalespriWithGst()+"");
//                            params.put("totalpayment", totalprice+"");
//                            params.put("stockid", pdd.getStockid()+"");
//
//                            new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
//                                @Override
//                                public void onSuccess(String data) {
//                                    // swipeContainer.setRefreshing(false);
//                                    pg.dismiss();
//
//                                    Log.e("LoginResponse", data);
//
//                                    if (data != null) {
//
//                                       UserAlerts.showAlert(data,OrderDetailsActivity.this);
//
//
//                                    }
//
//                                }
//
//                                @Override
//                                public void onFailure(String err) {
//                                    //  progressFragment.dismiss();
//                                    pg.dismiss();
//                                    //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();
//
//                                }
//                            }, ServerData.updateReturnItems + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile), Request.Method.GET).submitRequest();
//
//
//
//                        }




                    } else {

                        UserAlerts.showAlert("Please select products to return", OrderDetailsActivity.this);


                    }


                } else {

                    UserAlerts.showAlert("First show the bill to customer and collect money", OrderDetailsActivity.this);
                }

            }
        });

        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<ProductOrderData> prd = productOrderAdapter.getSelectedProductOrders();

                Log.d("ProductOrder", prd.size() + "");

                if (prd.size() > 0) {
                    code = code + 1;

                    List<ProductOrderData> prd_all = productOrderAdapter.getAllProductOrders();
                    List<ProductOrderData> prd_notselected = new ArrayList<>();
                    boolean anyqtyreturn=false;

                    if (prd_all.size() > 0) {

                        for (ProductOrderData pr : productOrderAdapter.getAllProductOrders()
                        ) {

                            if (pr.getSelected() == 1) {

                                int qty = Integer.parseInt(pr.getQty());

                                int qty_updated = pr.getQuantity_updated();

                                if(pr.getQuantity_updated()>qty)
                                {
                                    code = 0;
                                    pr_readytoUpdate.clear();
                                    UserAlerts.showAlert("Updated quantity must be less than of ordered quantity in " + pr.getProductName(), OrderDetailsActivity.this);
                                    break;

                                }

                                if(pr.getQuantity_updated()<qty)
                                {

                                    int q=qty-pr.getQuantity_updated();
                                    pr.setQtytobill(q);

                                    pr_readytoUpdate.add(pr);


                                }
                                else{

                                    int q=qty;

                                    pr.setQtytobill(q);
                                    pr_readytoUpdate.add(pr);
                                }


                            }


                        }

                        for (ProductOrderData pr : prd_all) {


                            int qty=Integer.parseInt(pr.getQty());

                            if(pr.getQuantity_updated()>qty)
                            {
                                code = 0;
                                prd_notselected.clear();
                                UserAlerts.showAlert("Updated quantity must be less than of ordered quantity in " + pr.getProductName(), OrderDetailsActivity.this);
                                break;

                            }

                            if(pr.getQuantity_updated()<qty)
                            {

                                int q=qty-pr.getQuantity_updated();
                                pr.setQtytobill(q);

                                prd_notselected.add(pr);


                            }
                            else{

                                if(pr.getSelected()==0) {

                                    int q = qty;

                                    pr.setQtytobill(q);
                                    prd_notselected.add(pr);
                                }
                            }


                           // if (pr.getSelected() == 0) {
                               // prd_notselected.add(pr);


                           // }


                        }


                    }


                    if (prd_notselected.size() > 0) {

                        ProgressFragment pg = new ProgressFragment();
                        pg.show(getSupportFragmentManager(), "");

                        Map<String, String> params = new HashMap<>();
                        params.put("timestamp", new Date().toString());


                        new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
                            @Override
                            public void onSuccess(String data) {
                                // swipeContainer.setRefreshing(false);
                                pg.dismiss();

                                Log.e("LoginResponse", data);

                                if (data != null) {

                                    KolicaSettings kls = new GsonBuilder().create().fromJson(data, KolicaSettings.class);

                                    if (kls.getStatus() == 1) {

                                        String freedeliveryamount = "";
                                        String deliverycharge = "";

                                        if (kls.getData().size() > 0) {
                                            freedeliveryamount = kls.getData().get(0).getFreeDeliveryamount();
                                            deliverycharge = kls.getData().get(0).getDeliverycharge();


                                        }


                                        Dialog d = new Dialog(OrderDetailsActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
                                        d.setContentView(R.layout.layout_updatedbill);
                                        RecyclerView recycler = d.findViewById(R.id.recycler);
                                        Button btnshare = d.findViewById(R.id.btnshare);
                                        Button btncollectamount=d.findViewById(R.id.btncollectamount);
                                        TextView txtdeliverycharge = d.findViewById(R.id.txtdeliverycharge);
                                        TextView txtTotal = d.findViewById(R.id.txtTotal);
                                        TextView txtTotalAmountTopay = d.findViewById(R.id.txtTotalAmountTopay);

                                        t = 0;
                                        for (ProductOrderData prr : prd_notselected
                                        ) {
                                            double p = Double.parseDouble(prr.getSalespriWithGst());

                                            double totalprice = p * prr.getQuantity_updated();

                                            t = t + totalprice;

                                        }

                                        deliverychargeamount = 0;
                                        double fdeliveryamount = Double.parseDouble(freedeliveryamount);
                                        double amounttopay = 0;

                                        if (t < fdeliveryamount) {

                                            deliverychargeamount = Double.parseDouble(deliverycharge);

                                            amounttopay = t + deliverychargeamount;
                                        } else {


                                            deliverychargeamount = 0;

                                            amounttopay = t + deliverychargeamount;
                                        }


                                        txtTotal.setText(" Total amount : " + t + " ₹");

                                        txtdeliverycharge.setText(" Delivery charge : " + deliverychargeamount + " ₹");

                                        txtTotalAmountTopay.setText(" Amount to pay : " + amounttopay + " ₹");


                                        BillOrderAdapter billOrderAdapter = new BillOrderAdapter(OrderDetailsActivity.this, prd_notselected);
                                        recycler.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this));
                                        recycler.setAdapter(billOrderAdapter);

                                        btnshare.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                d.dismiss();
                                            }
                                        });

                                        btncollectamount.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {

                                                new  AlertDialog.Builder(OrderDetailsActivity.this)
                                                        .setTitle(R.string.app_name)
                                                        .setMessage("Customer confirmed the amount ?")
                                                        .setCancelable(false)
                                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                               // new PreferenceHelper(OrderDetailsActivity.this).putData(ServerData.userkey,"");
                                                                dialog.dismiss();

                                                                d.dismiss();

                                                                showAmountDialog(t,deliverychargeamount,1);

                                                                // Whatever...
                                                            }
                                                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {

                                                        dialogInterface.dismiss();
                                                    }
                                                }).show();

                                                d.dismiss();

                                            }
                                        });


                                        d.show();


                                    } else {


                                    }


                                }

                            }

                            @Override
                            public void onFailure(String err) {
                                //  progressFragment.dismiss();
                                pg.dismiss();
                                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

                            }
                        }, ServerData.getSettingsData + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile), Request.Method.GET).submitRequest();


                        //generate pdf file and share it


                    } else {

                        UserAlerts.showAlert("No products remain for billing", OrderDetailsActivity.this);


                    }


                } else {

                    UserAlerts.showAlert("Please select products to return", OrderDetailsActivity.this);


                }


            }
        });


        dialog.setContentView(view);

        dialog.show();


    }

    public void showProductList() {

        final Dialog dialog = new Dialog(OrderDetailsActivity.this, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        View view = getLayoutInflater().inflate(R.layout.layout_prlist, null);

        RecyclerView rv = (RecyclerView) view.findViewById(R.id.rv);

        rv.setLayoutManager(new LinearLayoutManager(OrderDetailsActivity.this));
        rv.setAdapter(new ProductOrderAdapter(OrderDetailsActivity.this, prdata, 0));


        dialog.setContentView(view);

        dialog.show();


    }


    public void getOrderDetails() {

        ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "sdjfn");

        Map<String, String> params = new HashMap<>();
        params.put("timestamp", new Date().toString());


        new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //  swipeContainer.setRefreshing(false);
                progressFragment.dismiss();

                Log.e("LoginResponse", data);

                if (data != null) {

                    prdo = new GsonBuilder().create().fromJson(data, ProductOrder.class);

                    if (prdo.getStatus() == 1) {

                        if (prdo.getData().size() > 0) {

                            txtUsername.setText(prdo.getData().get(0).getCustomerName());
                            txtPhone.setText(prdo.getData().get(0).getPhone1());

                            prdata.clear();

                            prdata.addAll(prdo.getData());


                        }

                    } else {

                        UserAlerts.showAlert("Something wrong", OrderDetailsActivity.this);
                    }


                    // Toast.makeText(OrderDetailsActivity.this,data,Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();
                // swipeContainer.setRefreshing(false);
                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, ServerData.getSalesOrderItems + "/" + orderData.getId() + "/" + orderData.getUserid() + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile), Request.Method.GET).submitRequest();


    }


    public void getAddressDetails() {

        ProgressFragment progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "sdjfn");

        Map<String, String> params = new HashMap<>();
        params.put("timestamp", new Date().toString());


        new RequestHandler(OrderDetailsActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {
                //  swipeContainer.setRefreshing(false);
                progressFragment.dismiss();

                Log.e("LoginResponse", data);

                if (data != null) {

                    UserAddress userAddress = new GsonBuilder().create().fromJson(data, UserAddress.class);

                    if (userAddress.getStatus() == 1) {

                        if (userAddress.getData().size() > 0) {

                            latitude = userAddress.getData().get(0).getLatitude();
                            longitude = userAddress.getData().get(0).getLongitude();

                            txtAddress.setText(userAddress.getData().get(0).getName() + ",\n" +
                                    userAddress.getData().get(0).getHouseno() + ",\n" +
                                    userAddress.getData().get(0).getArea() + ",\n" +
                                    userAddress.getData().get(0).getPincode() + ",\n" +
                                    userAddress.getData().get(0).getAddresstype() + ",\n" +
                                    userAddress.getData().get(0).getTown());

                        }


                    } else {

                        UserAlerts.showAlert("Did not fetch address", OrderDetailsActivity.this);

                    }


                    // Toast.makeText(OrderDetailsActivity.this,data,Toast.LENGTH_SHORT).show();


                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();
                // swipeContainer.setRefreshing(false);
                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, ServerData.getAddressById + "/" + orderData.getAddressid() + "/" + new PreferenceHelper(OrderDetailsActivity.this).getData(ServerData.mobile), Request.Method.GET).submitRequest();


    }


}


class DecimalDigitsInputFilter implements InputFilter {
    private Pattern mPattern;

    DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
        mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        Matcher matcher = mPattern.matcher(dest);
        if (!matcher.matches())
            return "";
        return null;
    }
}