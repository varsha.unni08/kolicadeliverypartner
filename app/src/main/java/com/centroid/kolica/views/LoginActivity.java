package com.centroid.kolica.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.centroid.kolica.R;
import com.centroid.kolica.UserAlerts;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.preferencehelper.PreferenceHelper;
import com.centroid.kolica.progress.ProgressFragment;
import com.centroid.kolica.serverHandler.RequestHandler;
import com.centroid.kolica.serverHandler.ResponseHandler;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText edtPhone,edtPassword;
    Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        edtPhone=findViewById(R.id.edtPhone);
        edtPassword=findViewById(R.id.edtPassword);
        btnlogin=findViewById(R.id.btnlogin);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtPhone.getText().toString().equalsIgnoreCase(""))
                {
                    if(!edtPassword.getText().toString().equalsIgnoreCase(""))
                    {

getUserDetails();

                    }
                    else{


                        UserAlerts.showAlert("Enter Password",LoginActivity.this);

                    }


                }
                else{


                    UserAlerts.showAlert("Enter Phone number",LoginActivity.this);

                }




            }
        });
    }



    public void getUserDetails()
    {
        ProgressFragment progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfn");



        Map<String,String> params=new HashMap<>();
        params.put("timestamp",new Date().toString());


        new RequestHandler(LoginActivity.this, params, new ResponseHandler() {
            @Override
            public void onSuccess(String data) {

                progressFragment.dismiss();

                Log.e("LoginResponse",data);

                if(data!=null)
                {

                    try{

                        JSONObject jsonObject=new JSONObject(data);

                        if(jsonObject.getInt("status")==1)
                        {


                            String token=jsonObject.getString("userid");
                            new PreferenceHelper(LoginActivity.this).putData(ServerData.mobile,edtPhone.getText().toString());


                            new PreferenceHelper(LoginActivity.this).putData(ServerData.userkey,token);

                            startActivity(new Intent(LoginActivity.this, OrdersActivity.class));
                            finish();

                        }
                        else {

                            // Toast.makeText(LoginActivity.this,"Login failed",Toast.LENGTH_SHORT).show();
                        }



                    }catch (Exception e)
                    {

                    }




                }

            }

            @Override
            public void onFailure(String err) {
                progressFragment.dismiss();

                //Toast.makeText(context,err,Toast.LENGTH_SHORT).show();

            }
        }, ServerData.login+"/"+edtPhone.getText().toString()+"/"+edtPassword.getText().toString(), Request.Method.GET).submitRequest();



    }
}