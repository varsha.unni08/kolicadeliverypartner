package com.centroid.kolica.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.centroid.kolica.R;
import com.centroid.kolica.constants.ServerData;
import com.centroid.kolica.preferencehelper.PreferenceHelper;

public class SplashActivity extends AppCompatActivity {

    Thread t=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        t=new Thread(new Runnable() {
            @Override
            public void run() {

                try{

                    t.sleep(3000);

                    if(!new PreferenceHelper(SplashActivity.this).getData(ServerData.userkey).equalsIgnoreCase(""))
                    {
                        startActivity(new Intent(SplashActivity.this, OrdersActivity.class));
                        finish();

                    }
                    else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                        finish();

                    }


                }catch (Exception e)
                {

                }

            }
        });

        t.start();
    }
}